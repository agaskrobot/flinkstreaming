package java;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class FlinkStreaming {
    StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    DataStream<String> inputURLs = env.fromElements("http://opendata-ajuntament.barcelona.cat/data/en/dataset/bicing/resource/62c0c9cf-c104-4105-8e0d-6f483d39c0b2");

inputURLs.map(new MapFunction<String, String>() {
        @Override
        public String map(String s) throws Exception {
            URL url = new URL(s);
            InputStream is = url.openStream();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

            StringBuilder builder = new StringBuilder();
            String line;

            try {
                while ((line = bufferedReader.readLine()) != null) {
                    builder.append(line + "\n");
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

            try {
                bufferedReader.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

            return builder.toString();
        }
    }).print();

env.execute("URL download job");
}
